(function () {
    'use strict';
    // Declare app level module which depends on views, and components
    var myApp = angular.module('myApp', [
        'ngRoute',
        'myApp.controllers'
    ]);

    myApp.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
                when ('/home', {
                templateUrl:'app/partials/landing.html'
            }).
                when ('/services', {
                templateUrl:'app/partials/services.html'
            }).
                when ('/photos', {
                templateUrl:'app/partials/photos.html'
            }).
                when ('/contact', {
                templateUrl:'app/partials/contact.html'
            }).
            otherwise({redirectTo: '/home'});
    }]).run(function () {
        console.log("app is started");
    });

    angular.module('myApp.controllers', []);
})();