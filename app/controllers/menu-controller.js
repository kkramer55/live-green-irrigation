(function () {
    'use strict';
    var module = angular.module('myApp.controllers');

    module.controller('menuCtrl', function ($scope) {
        $scope.menus = [
            {
                "name": "HOME",
                "route" : "landing"
            },
            {
                "name": "SERVICES",
                "route" : "services"
            },
            {
                "name": "PHOTOS",
                "route" : "photos"
            },
            {
                "name": "CONTACT",
                "route" : "contact"
            }
        ]});
})();


